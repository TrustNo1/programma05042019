// Scrivere un programma file.c che chieda quante righe scrivere e per ogni riga
// poi acquisisca una stringa la scriva nel file
//
// 1.chiedere all'utente quante righe inserire
// 2.aprire un file
// 3.fare un ciclo con l'inserimento della stringa
// 4.scrivere la stringa acquisita nel file
// 5.uscire dal ciclo e chiudere il file

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#define _DEBUG_ 1
#define TRACE (_DEBUG_==0)?(void)0 : printf
#define EOL "\r\n"
#define PATH

static FILE* id_file; // variabile GLOBALE visibile all'interno di tutto il file c

int file_open(char* file_name);
int file_close(void);

int read_integer (void);

int read_integer (void)
{
char str[10]={0};

printf ("Inserisci il numero di righe: ");
scanf ("%s", &str[0]);

//sanity check 
for(i=0; i<strlen(&str[0]); i++)
{ 
   if(((str[i]>='0')&&(str[i]<='9'))==0)
       break;


}
if (i!=strlen(&str[0]))
   return -1;
else
   return atoi(&str[0]);        

}




int file_open(char* file_name)
{
    TRACE("This function is %s"EOL,__FUNCTION__); //la macro __FUNCTION__ resitituisce una stringa
// il cui contenuto e' il nome della funzione in cui viene invocata

    id_file=fopen(&file_name[0], "r+");
    if(id_file==NULL)
    {
       printf("Errore %d nell'apertura del file %s" EOL, errno, file_name);
       if (errno==ENOENT)
       {
          printf("File inesistente" EOL);
       }
       return errno;
    }

return 0; //OK!

}//int file_open(char* file_name)

int file_close(void)
{
int retv=0; 
   TRACE("This function is %s"EOL,__FUNCTION__); //la macro __FUNCTION__ resitituisce una stringa

   retv=fclose(id_file);
  if retv==0
     return 0;  
  else
     return -1;

}//int file_close(void)




int main (int argc, char *argv[])
{
int righe
int i;
int retv=0;
char file_name[20];


   retv=file_open("prova_file.txt");
   if(retv!=0)
   {
      printf("Error"EOL);
      exit(0);
   }

   while(read_integer()<0)
   {
      printf("Error hai sbagliato ...ripeti"EOL);
   }




 
   retv=file_close();
   if(retv!=0)
   {
      printf("Error"EOL);
      exit(0);
   }
 

return 0;
}

